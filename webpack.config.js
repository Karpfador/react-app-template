const path = require('path')
const webpack = require('webpack')

module.exports = {
    entry: './src/index.tsx',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            { test: /\.tsx?$/, loader: 'ts-loader' },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
    },
    plugins: [new webpack.HotModuleReplacementPlugin()],
    devServer: {
        static: './',
        hot: true,
    },
}
